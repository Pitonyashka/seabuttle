from telebot import types
import telebot

import sheet
import query
import generate


bot = telebot.TeleBot('5552888812:AAFLvgwi10rl8YkQBXQkTJheR9R7ofxqnCI')
prize = {
        None: ['ничего, в другой раз попадёшь в цель🔥'],
        'р.10': ['10 рублей', '😉'],
        'П': ['Подписка на месяц', '😉'],
        'С500': ['Сертификат на 500 рублей', '😉'],
        'С1000': ['Сертификат на 1000 рублей', '😉'],
        'С2000': ['Сертификат на 2000 рублей', '😉'],
        'С3000': ['Сертификат на 3000 рублей', '😉'],
        'СБ': ['Симбионты (2 банки)', '🚀'],
        'СБТ': ['Симбионты (2 банки)', '🚀'],
        'МК': ['Урок танцев', '😍'],
        'Ф': ['Фотосессия', '🔥'],
        'М': ['Сертификат на массаж', '🔥'],
        'СК': ['Сертификат на маникюр или брови', '🙌🏼'],
        'К': ['2 билета в кино', '🔥'],
        'Д': ['Доставка пицца/суши', '🍱🍕'],
        'ОТЕЛЬ': ['Ночь в отеле', '🔥😱']
    }


def is_valid(text):

    text = text.lower()

    first_symbol = 'абвгдежзиклмнопрстуф'
    sheet_symbol = 'bcdefghijklmnopqrstu'

    if len(text) == 2 or len(text) == 3:
        second_symbol = text[1:]
        if text[0] in first_symbol and second_symbol.isdigit():
            if 0 < int(second_symbol) < 21:
                coordinates = [
                    f'{sheet_symbol[first_symbol.find(text[0])]}{str(int(second_symbol) + 1)}',
                    [int(second_symbol), first_symbol.find(text[0]) + 1]
                ]
                return coordinates


def send_map(chat_id, description=True):
    generate.generate()
    photo = open('empty.png', 'rb')
    bot.send_photo(chat_id, photo)
    if description:
        test = f'Обозначения:\n🔵 - закрытое поле\n🟡 - открытое поле\n🟢 - однопалубный корабль\n🔴 - двухпалубный корабль\nЕсли что-то непонятно, всегда можешь обратиться за помощью 😉'
        bot.send_message(chat_id, test)


@bot.message_handler(commands=['map'])
def start_message(message):
    send_map(message.chat.id)


@bot.message_handler(commands=['prize'])
def start_message(message):
    test = ''
    for i in prize:
        test += f'{i}: {prize[i][0]},\n'
    test += f'\nПримечание:\nПодписки на месяц:\nКинопоиск\nStorytel\nОкко\nМузыка\nЮтуб\n\nСертификаты в магазины:\n1. М видео\n2. Золотое Яблоко\n3. Летуаль\n4. Озон\n5. Буквоед\nНоминал: 500/1000/2000/3000\n\nМастер-класс: урок танцев'
    bot.send_message(message.chat.id, test)


@bot.message_handler(commands=['help'])
def start_message(message):
    test = 'С моей помощью ты можешь играть в морской бой и выигрывать ценные призы!\nЕсли у тебя что-то не получается или что-то не понятно пиши сюда 👉🏻 @ananasteisha'
    bot.send_message(message.chat.id, test)


@bot.message_handler(commands=['start'])
def start_message(message):
    if message.chat.id < 0:
        test = f'Приветсвую, {message.from_user.first_name}! Давай начнём игру?\nЖелаю удачи!🔥'
        bot.send_message(message.chat.id, test)
    else:
        if query.check_user(message.from_user.id):
            text = f'Приветсвую, {message.from_user.first_name}! Давай начнём игру?\nЖелаю удачи!🔥'
            bot.send_message(message.chat.id, text)
        else:
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton("Регистрация")
            markup.add(btn1)
            text = f'Приветсвую, {message.from_user.first_name}! Я бот который будет помогать тебе играть в морской бой! Но для начала ты должен зарегестрироваться!'
            bot.send_message(message.chat.id, text, reply_markup=markup)



@bot.message_handler(content_types=["text"])
def repeat_all_messages(message): # Название функции не играет никакой роли
    if message.from_user.id == 559817434:
        if message.text.lower().startswith('limit') and message.text[6:].isdigit():
            query.set_limit(int(message.text[6:]))
            bot.send_message(message.chat.id, str(f'Установлен новый лимит: {message.text[6:]}'))
        elif message.text.lower() == 'get_limit':
            bot.send_message(message.chat.id, str(f'Текущий лимит: {str(query.get_limit()[0][0])}'))
    if message.chat.id > 0:
        if message.text.lower() == 'регистрация':
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton("Призы")
            btn2 = types.KeyboardButton("Статистика")
            btn3 = types.KeyboardButton("Мои выстрелы")
            markup.add(btn1, btn2, btn3)
            if query.check_user(message.from_user.id):
                text = 'Вы уже зарегестрированы'
            else:
                query.create_user(message.from_user.id, message.from_user.first_name, message.from_user.last_name,)
                text = 'Регистрация прошла удачно.\nТеперь вы можете зарабатывать выстрелы и тратить их, чтобы получить возможность выиграть разные призы!'
            bot.send_message(message.chat.id, text, reply_markup=markup)
        elif message.text.lower() == 'мои выстрелы':
            limit = query.get_user_shot(message.from_user.id)
            text = f'Ты можешь выстрелить {limit} раз'
            bot.send_message(message.chat.id, text)
        elif message.text.lower() == 'призы':
            mod_prize = query.get_all_prize()
            text = 'Ты ещё можешь выиграть:\n\n'
            for i in mod_prize:
                text += f'{i[1]}: {i[3]}\n'
            bot.send_message(message.chat.id, text)
        elif message.text.lower() == 'статистика':
            text = ''
            text += f'Количество совершенных выстрелов: {query.get_user_stat(message.from_user.id)}'
            text += f'\n\nТвои призы:\n'
            for i in query.get_user_prize(message.from_user.id):
                text += f'{i[0]}\n'
            if not query.get_user_prize(message.from_user.id):
                text += "\nТы пока ничего не выиграл👀\nНо у тебя всё в переди🤗"
            else:
                text += '\nПоздравляю 🥳🥳🥳'
            bot.send_message(message.chat.id, text)
    else:
        coor = is_valid(message.text)
        if coor:
            if query.check_user(message.from_user.id):
                limit = query.get_limit(message.from_user.id)
                if limit > 0:
                    status_cell = sheet.get_items(coor)
                    if status_cell != 'Ячейка занята':
                        text = f"{message.from_user.first_name}, твой приз {status_cell if status_cell is None else prize[status_cell[0][0]][0]}{'' if status_cell is None else prize[status_cell[0][0]][1]}\nПоздравляем!😉\nС тобой свяжется София для получения приза 🔥"
                        if status_cell[0][0] == 'р.10':
                            text = f"{message.from_user.first_name}, твой приз {status_cell if status_cell is None else prize[status_cell[0][0]][0]}{'' if status_cell is None else prize[status_cell[0][0]][1]}\nПоздравляем!😉\nМы учтём и прибавим сумму к заработной плате 🔥"
                        query.sub_limit(1, message.from_user.id)
                        query.add_shot(message.text.lower())
                        if query.is_double_ship(message.text.lower())[0][0]:
                            double_ship = query.get_double_ship(message.text.lower())[0]
                            if not query.is_dead_double_ship(double_ship[1], double_ship[2]):
                                text = f'Ты попал в двухпалубный корабль!🚀Ты можешь выиграть {status_cell if status_cell is None else prize[status_cell[0][0]][0]}!🔥\nОсталось добить его! Just do it!'
                            else:
                                query.sub_double_prize(message.text.lower())
                                query.add_user_double_prize(message.from_user.id, message.text.lower())
                        elif query.is_ship(message.text.lower()):
                            query.sub_prize(message.text.lower())
                            query.add_user_prize(message.from_user.id, message.text.lower())
                        query.add_user_stat(message.from_user.id)
                    else:
                        text = 'В эту ячейку уже стреляли, попробуйте выстрелить в другую ячейку!'
                    send_map(message.chat.id, description=False)
                else:
                    text = 'Твои выстрелы закончились, ждём твои достижения для новых выстрелов 🚀'
            else:
                text = 'Только зарегестрированные пользователи могут стрелять'
            bot.send_message(message.chat.id, str(text))
        elif (message.text.lower().find('покажи') != -1 or message.text.lower().find('открой') != -1 or message.text.lower().find('посмотреть') != -1 or message.text.lower().find('отправь') != -1) and (message.text.lower().find('карту') != -1 or message.text.lower().find('поле') != -1):
            send_map(message.chat.id)


if __name__ == '__main__':
     bot.infinity_polling()

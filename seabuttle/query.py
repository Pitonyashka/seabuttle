import sqlite3


def get_limit(user_id):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT "limit" FROM main_botuser where user_id = ?;''', (user_id,))
		x = cursor.fetchall()[0][0]
	return x

def set_limit(value):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''UPDATE "Limit" SET "values" = ? WHERE "id" = 1;''', (value,))
		x = cursor.fetchall()


def add_limit(value):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''UPDATE "Limit" SET "values" = "values" + ? WHERE "id" = 1;''', (value,))
		x = cursor.fetchall()


def sub_limit(value, user_id):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''UPDATE main_botuser SET "limit" = "limit" - ? WHERE user_id = ?;''', (value, user_id))


def add_shot(cell):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''INSERT INTO "shot"("cell") VALUES (?)''', (cell,))


def check_shot(cell):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT EXISTS(SELECT "id" FROM "shot" WHERE "cell" = ?)''', (cell,))
		x = cursor.fetchall()
	return x


def is_double_ship(cell):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute(
			'''SELECT EXISTS(SELECT * FROM "double_ship" WHERE "first_cell" = ? or "second_cell" = ?)''', 
			(cell, cell)
		)
		x = cursor.fetchall()
	return x


def get_double_ship(cell):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute(
			'''SELECT * FROM "double_ship" WHERE "first_cell" = ? or "second_cell" = ?''', 
			(cell, cell)
		)
		x = cursor.fetchall()
	return x


def is_dead_double_ship(cell1, cell2):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT EXISTS(SELECT "id" FROM "shot" WHERE "cell" = ?)''', (cell1,))
		first_shot = cursor.fetchall()[0][0]
		cursor.execute('''SELECT EXISTS(SELECT "id" FROM "shot" WHERE "cell" = ?)''', (cell2,))
		second_shot = cursor.fetchall()[0][0]
		return first_shot and second_shot


def check_user(user_id):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT EXISTS(SELECT "id" FROM "main_botuser" WHERE "user_id" = ?)''', (user_id,))
		x = cursor.fetchall()[0][0]
		return x


def create_user(user_id, first_name, last_name):
	first_name = '' if not first_name else first_name
	last_name = '' if not last_name else last_name
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''INSERT INTO "main_botuser"("user_id", "first_name", "last_name", "limit", "admin", "shots") VALUES (?, ?, ?, 0, 0, 0)''', (user_id, first_name, last_name))


def get_user_shot(user_id):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT "limit" FROM main_botuser WHERE user_id = ? ''', (user_id,))
		x = cursor.fetchall()[0][0]
		return x


def is_ship(coor):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT EXISTS(SELECT "id" FROM "main_ship" WHERE "coor" = ?)''', (coor,))
		x = cursor.fetchall()[0][0]
		return x


def get_prize_ship(coor):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT main_prize.reduction FROM main_ship JOIN main_prize ON main_ship.prize_id = main_prize.id WHERE main_ship.coor = ?;''', (coor,))
		x = cursor.fetchall()[0][0]
		return x


def sub_prize(coor):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''UPDATE main_prize SET count = count - 1 WHERE id in (SELECT main_prize.id FROM main_ship JOIN main_prize ON main_ship.prize_id = main_prize.id WHERE main_ship.coor = ?);''', (coor,))


def get_all_prize():
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT * FROM main_prize WHERE count > 0;''')
		x = cursor.fetchall()
		return x


def get_user_prize(user_id):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT main_prize.title From main_userprize JOIN main_prize ON main_userprize.prize_id = main_prize.id WHERE main_userprize.user_id in (SELECT id from main_botuser where user_id = ?);''', (user_id,))
		x = cursor.fetchall()
		return x


def get_user_stat(user_id):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT shots FROM main_botuser where user_id = ?;''', (user_id,))
		x = cursor.fetchall()[0][0]
		return x


def add_user_stat(user_id):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''UPDATE main_botuser SET shots = shots + 1 WHERE user_id = ?;''', (user_id,))


def add_user_prize(user_id, coor):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT id FROM main_botuser WHERE user_id = ?;''', (user_id,))
		user = cursor.fetchall()[0][0]
		cursor.execute('''SELECT main_prize.id FROM main_ship JOIN main_prize ON main_ship.prize_id = main_prize.id WHERE main_ship.coor = ?;''', (coor,))
		prize = cursor.fetchall()[0][0]
		cursor.execute('''INSERT INTO main_userprize(prize_id, user_id) values (?, ?)''', (prize, user))


def sub_double_prize(cell):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''UPDATE main_prize SET count = count - 1 WHERE id in (SELECT main_prize.id FROM double_ship JOIN main_prize ON double_ship.prize_id = main_prize.id WHERE double_ship.first_cell = ? or double_ship.second_cell=?);''', (cell, cell))


def add_user_double_prize(user_id, cell):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT id FROM main_botuser WHERE user_id = ?;''', (user_id,))
		user = cursor.fetchall()[0][0]
		cursor.execute('''SELECT main_prize.id FROM double_ship JOIN main_prize ON double_ship.prize_id = main_prize.id WHERE double_ship.first_cell = ? or double_ship.second_cell = ?;''', (cell, cell))
		prize = cursor.fetchall()[0][0]
		cursor.execute('''INSERT INTO main_userprize(prize_id, user_id) values (?, ?)''', (prize, user))


def get_prize_double_ship(coor):
	with sqlite3.connect('buttle.sqlite3') as db:
		cursor = db.cursor()
		cursor.execute('''SELECT main_prize.reduction FROM double_ship JOIN main_prize ON double_ship.prize_id = main_prize.id WHERE double_ship.first_cell = ? or double_ship.second_cell = ?;''', (coor, coor))
		x = cursor.fetchall()[0][0]
		return x

from PIL import Image, ImageColor, ImageFont
from PIL import ImageDraw
import query


def generate():
    width = 500
    height = 500
    symbol = 'абвгдежзиклмнопрстуф'

    image = Image.new("RGB", (width, height))

    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype('Roboto-Regular.ttf', size=18)

    for i in range(21):
        x = int(width/21 * i)
        draw.line((x, 0, x, height), fill=ImageColor.getrgb("white"))
        draw.line((0, x, width, x), fill=ImageColor.getrgb("white"))

    for i in range(1, 21):
        for j in range(1, 21):
            is_coor = False
            color = "cyan"
            if query.check_shot(symbol[i-1]+str(j))[0][0]:
                color = "yellow"
                if query.is_double_ship(symbol[i-1]+str(j))[0][0]:
                    color = "red"
                    coor = query.get_prize_double_ship(symbol[i-1]+str(j))
                    is_coor = True
                elif query.is_ship(symbol[i-1]+str(j)):
                    color = "green"
                    coor = query.get_prize_ship(symbol[i-1]+str(j))
                    is_coor = True
            draw.rectangle((1 + 500/21*i, 1 + 500/21*j , 21 + 500/21*i, 21 + 500/21*j), fill=ImageColor.getrgb(color))
            if is_coor:
                draw.text(
                    (5+500/21*i,2+500/21*j),
                    coor,
                    fill=('white'),
                    font=font
                )

        draw.text(
            (8 ,5+500/21*i),
            str(i),
            fill=('white')
        )
        draw.text(
            (8+500/21*i,5),
            symbol[i-1],
            fill=('white'),
            font=font
        )

    image.save("empty.png", "PNG")

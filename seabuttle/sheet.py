import os

import httplib2
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


def get_service_sacc():
    """
    Могу читать и (возможно) писать в таблицы кот. выдан доступ
    для сервисного аккаунта приложения
    :return:
    """
    creds_json = os.path.dirname(__file__) + "/auth.json"
    scopes = ['https://www.googleapis.com/auth/spreadsheets']

    creds_service = ServiceAccountCredentials.from_json_keyfile_name(creds_json, scopes).authorize(httplib2.Http())
    return build('sheets', 'v4', http=creds_service)


def is_checked(sheet, cell, sheet_id):
    colors = sheet.get(
        spreadsheetId=sheet_id,
        ranges=[f"Лист1!{cell}:{cell}"],
        includeGridData=True
    ).execute()['sheets'][0]['data'][0]['rowData'][0]['values'][0]['effectiveFormat']['backgroundColorStyle']['rgbColor']
    return False if colors['red'] == 1 and colors['green'] == 1 and colors['blue'] == 1 else True


def get_items(cell):
    # service = get_service_simple()
    service = get_service_sacc()
    sheet = service.spreadsheets()

    # https://docs.google.com/spreadsheets/d/xxx/edit#gid=0
    sheet_id = "1_34FxNSOkbDbYeccRbmaektF_qDv1BroQsWMT-GEC9w"

    # https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/get
    if is_checked(sheet, cell[0], sheet_id):
        resp = sheet.values().get(spreadsheetId=sheet_id, range=f"Лист1!{cell[0]}:{cell[0]}").execute()

        # https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/batchGet
        #resp = sheet.values().batchGet(spreadsheetId=sheet_id, ranges=["Лист1!A1:Z99"]).execute()
        sheet.batchUpdate(
            spreadsheetId = sheet_id,
            body = {
                "requests": [{
                    "repeatCell": {
                        "cell": {
                            "userEnteredFormat": {
                                "backgroundColor": {
                                    "red": 1,
                                    "green": 1,
                                    "blue": 1,
                                    "alpha": 0
                                },
                            }
                        },
                        "range": {
                            "sheetId": 0,
                            "startRowIndex": cell[1][0],
                            "endRowIndex": cell[1][0] + 1,
                            "startColumnIndex": cell[1][1],
                            "endColumnIndex": cell[1][1] + 1
                        },
                        "fields": "userEnteredFormat"
                    }
                }]
            }
        ).execute()

        return resp.get('values', None)
    else:
        return 'Ячейка занята'

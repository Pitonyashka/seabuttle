from django.contrib import admin

from main import models

# Register your models here.
admin.site.register(models.BotUser)
admin.site.register(models.Prize)
admin.site.register(models.Ship)
admin.site.register(models.UserPrize)

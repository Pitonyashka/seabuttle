from django.db import models

# Create your models here.
class BotUser(models.Model):

	user_id = models.IntegerField('id телеграмм аккаунта')
	first_name = models.CharField('Имя пользователя', max_length=255)
	last_name = models.CharField('Фамилия', max_length=255, blank=True, null=True)
	limit = models.IntegerField('Лимит на выстрелы')
	admin = models.BooleanField('Администратор', default=False)
	shots = models.IntegerField('Кол-во сделанных выстрелов', default=0)

	class Meta:
		verbose_name = 'Участнник'
		verbose_name_plural = 'Участники'

	def __str__(self):
		return f'{self.first_name} {self.last_name}'


class Prize(models.Model):

	title = models.CharField('Наименование', max_length=255)
	reduction = models.CharField('Сокращение', max_length=10)
	count = models.IntegerField('Кол-во', default=0)

	class Meta:
		verbose_name = 'Приз'
		verbose_name_plural = 'Призы'

	def __str__(self):
		return self.title


class Ship(models.Model):

	coor = models.CharField('Координата', max_length=5, unique=True)
	prize = models.ForeignKey(Prize, on_delete=models.CASCADE)

	class Meta:
		verbose_name = 'Корабль'
		verbose_name_plural = 'Корабли'

	def __str__(self):
		return self.coor


class UserPrize(models.Model):

	prize = models.ForeignKey(Prize, on_delete=models.CASCADE)
	user = models.ForeignKey(BotUser, on_delete=models.CASCADE)

	class Meta:
		verbose_name = 'Разыгранный приз'
		verbose_name_plural = 'Разыгранные призы'

	def __str__(self):
		return f'{self.user}: {self.prize}'
